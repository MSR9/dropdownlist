//
//  HeaderView.swift
//  Expand&CollapseSections
//
//  Created by EPITADMBP04 on 3/28/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import Foundation
import UIKit

protocol HeaderDelegate {
    func callHeader(index: Int)
}

class HeaderView : UIView {
    
    var sectionIndex: Int?
    var delegate: HeaderDelegate?
    
    override init(frame: CGRect) {
        super.init (frame: frame)
        self.addSubview(btn)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:)has not been implemented")
    }
    
    lazy var btn: UIButton = {
        let btn = UIButton(frame: CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: self.frame.width, height: self.frame.height))
        btn.backgroundColor = UIColor.magenta
        btn.titleLabel?.textColor = UIColor.white
        btn.layer.cornerRadius = 10
        btn.clipsToBounds = true
        btn.addTarget(self, action: #selector(onClickHeaderView), for: .touchUpInside)
        return btn
    }()

    @objc func onClickHeaderView() {
        if let index = sectionIndex {
            delegate?.callHeader(index: index)
        }
    }
}
