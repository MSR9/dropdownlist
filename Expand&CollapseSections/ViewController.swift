//
//  ViewController.swift
//  Expand&CollapseSections
//
//  Created by EPITADMBP04 on 3/28/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, HeaderDelegate {
    
    let cellIdentifier: String = "Cell"
    
    var data = [DataModel(headerName: "Fruits", subType: ["Banana","Mango","Guava"], isExpandable: false),
                DataModel(headerName: "Cars", subType: ["Benz","BMW","AlphaRomeo"], isExpandable: false),
                DataModel(headerName: "Countries", subType: ["USA","India","Australia"], isExpandable: false)]
    
    @IBOutlet weak var tblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblView.tableFooterView = UIView()
    }

    // MARK: - UITableView Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if data[section].isExpandable {
            return data[section].subType.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        cell?.textLabel?.text = data[indexPath.section].subType[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = HeaderView(frame: CGRect(x: 0, y: 16, width: self.tblView.frame.size.width, height: 40))
        headerView.delegate = self
        headerView.sectionIndex = section
        headerView.btn.setTitle(data[section].headerName, for: .normal)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if data [indexPath.section].isExpandable {
            return 40
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func callHeader(index: Int) {
        data[index].isExpandable = !data[index].isExpandable
        self.tblView.reloadSections([index], with: .automatic)
    }
}

